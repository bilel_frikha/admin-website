FROM node:14.15.4 AS admin-website-image
WORKDIR /opt/ng 
COPY package.json ./
RUN npm install
COPY . ./ 
RUN node_modules/.bin/ng build --prod
FROM nginx:alpine
WORKDIR /usr/share/nginx/html
COPY nginx.conf /usr/local/etc/nginx/nginx.conf
COPY dist/ .
